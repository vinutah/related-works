# related-works


[pre-defined sparsity for low-complexity cnns](https://arxiv.org/pdf/2001.10710.pdf)

[Automatic Pruning for Quantized Neural Networks](https://arxiv.org/pdf/2002.00523.pdf)

[Channel Pruning via Automatic Structure Search](https://arxiv.org/pdf/2001.08565.pdf)

[Identifying and Pruning Redundant Structures for Deep Neural Networks](https://ieeexplore.ieee.org/abstract/document/8966025)

[An Image Enhancing Pattern-based Sparsity for Real-time Inference on Mobile Devices](https://arxiv.org/pdf/2001.07710.pdf)

[Pruning CNN's with linear filter ensembles](https://arxiv.org/pdf/2001.08142.pdf)

[SS-Auto: A Single-Shot, Automatic Structured Weight Pruning Framework of DNNs with Ultra-High Efficiency](https://arxiv.org/pdf/2001.08839.pdf)

[An Image Enhancing Pattern-based Sparsity for Real-time Inference on Mobile Devices](https://arxiv.org/pdf/2001.07710.pdf)

# robustness-works

[CERTIFAI: Counterfactual Explanations for Robustness, Transparency, Interpretability, and Fairness of Artificial Intelligence models](https://arxiv.org/abs/1905.07857)


